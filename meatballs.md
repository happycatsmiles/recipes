# Meatballs

* 1 pound hamburger
* 3 tablespoons dried onion
* 1/2 cup brad crumbs
* 1 teaspoon salt
* 1/2 teaspoon Worcestershire sauce.
* 1 egg
* 1/4 cup milk

Bake at 400F for 10-15 minutes.

Bake on foil-lined pan for easier clean up.
