# Sun Pancakes

* 1/14 cups flour
* 2 teaspoons baking powder
* 1/2 teaspoon baking soda
* 1/4 cup powdered sugar **OR** 1 tablespoon regular sugar

Blend In:

* 1/2 stick butter (1/4 cup)
* 1 egg
* 1 cup milk

## Notes

* In practice, regular sugar. Have not tried powdered sugar.

Source: Grandma K.
