# Bannock

* 4 cups flour
* 1 tablespoon baking powder
* 1 teaspoon salt (optional)
* 1 1/2 cups milk
* 1 egg

Roll out flat in circles about 4-6 inches in diameter, like a pancake.
The dough is very thick, like bread dough, but you make bread cakes.
Sprinkle a little flour on a flat surface to roll out the dough so it won’t stick to everything,
but don’t use too much flour or it will taste like flour and get hard.

Cook on flat grill or frying pan until cooked through.
You may use a small amount of butter on the grill or pan, though this is optional.
They shouldn’t stick much but butter adds a little flavor.
They start to brown a little.
Flip them over and cook on both sides.

### Navajo Tacos

Put a bread on your plate.

Then put things like shredded cheese, spiced hamburger, refried beans, chilli, lettuce, diced tomatoes, salsa, guacamole, sour cream, etc. on top.
There’s no standard way to do this.
We lay out the ingredients and let people choose what they want to put on them.

### Trivia

This is a traditional lower class bread from Scotland.
It made its way to the Americas during its exploration and by trappers in the early 1800’s.
It was adopted by a number of indigenous people.

Source: Blackfoot student that Grandma K. & Grandpa L. hosted.
