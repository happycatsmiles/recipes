# Banana Cookies

## Cookie

* 3/4 cup butter
* 3/4 cup brown sugar
* 1 egg
* 2 cups flour
* 1 teaspoon vanilla
* 1 teaspoon baking soda
* 2 mashed ripe bananas

Bake at 350F for 10 minutes.

Can make all cookie doughs like a sheet cake on a cookie sheet.
Bake for 15-20 minutes. 

## Frosting

Bring the following to a boil:

* 3 tablespoons brown sugar
* 2 tablespoons milk
* 2 tablespoons butter

Add:

* 1 teaspoon vanilla

Mix in powdered sugar until the right consistency.
