# Brownies

## Dry Ingredients

* 1 cup flour
* 1 cup sugar
* 1/2 teaspoon baking powder

## Melt Together

* 1 tablespoon melted butter
* 3 tablespoons cocoa powder

## Other Ingredients

* 3 eggs
* 1 teaspoon vanilla extract
* Optional: chopped nuts, etc.

Mix dry ingredients first.

Add other ingredients in order.

Put in greased pan.

Bake 30 minutes at 350F.

Source: Grandma K.
