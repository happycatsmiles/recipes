# Baking Powder Biscuits

* 2 cups flour
* 3 teaspoons baking powder
* 1/2 teaspoons salt
* 1/4 cup shortening
* 2/3 to 3/4 cups milk

Mix dry ingredients together.
Cut in shortening until the consistency is like coarse crumbs.
Add milk all at once and stir with a fork until mixed well.

Knead 10 to 12 strokes.
Pat out 1/2" thick and cut into circles.

Brush tops with milk.

Bake at 450F for 15 to 30 minutes.
The amount of time required depends on the size of the biscuits.
The recipe says it makes 16,
which should be a smaller size.

## Drop Biscuits

To make drop biscuits,
increase milk to 1 cup.
Drop from teaspoon.

Bake at 450F for 12-15 minutes.

Source: Grandma K.
