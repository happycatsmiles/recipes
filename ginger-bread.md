# Ginger Bread

* 1 cup butter or margin
* 2 1/2 cup flour
* 1 1/2 cup palm sugar
* 1 teaspoon baking soda
* 2 teaspoon cinnamon
* 1/2 teaspoon ground cloves
* 1/2 nutmeg
* 2 teaspoon ground ginger
* 2 eggs
* 1/4 cup fructose or ½ cup agave

1. Cream butter fructose and sugar.
1. Beat in egg.
1. Mix dry ingredients together in separate bowl.
1. Add dry ingredients to creamed butter, a half cup at a time until fully combined.
1. Bake  at 375F for 15 to 25 minutes
