---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Ogórki Kiszone}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](pickled-cucumbers.jpg){width=40%}

# Składniki
- Koper
- Czosnek
- Chrzan
- Ogórki całe (Małe lub średnie)

# Zalewa
- 1L wody
- Kopiasta łyżka soli lub dwie płaskie

\End{multicols}

# Przygotowanie

Wymieszać wode z solą. Włożyć ogórki do słoików, tyle ile się zmieści, dodać trochę kopru, kilka ząbków czosnku i trochę chrzanu i zalać słoną wodą. Słoiki odstawić do ciepłego i ciemnego miejsca i odczekać przez kilka dni aż woda zmętnieje w środku, potem wynieść do zimnego miejsca.

# Notatki

Słoiki po zalaniu można okryć kocem i zostawić w kuchni lub innym pomieszczeniu, na przykład pod stołem. Należy pilnować by nie stały za długo po zmętnieniu wody bo mogą się zepsuć.

Nie ma "właściwych" proporcji jeśli chodzi o składniki, należy je dodać zależnie od uznania. Zalewe należy zrobić tyle ile potrzeba do zalania wszystkich ogórków w podanych proporcjach.

Jeśli użyje się połowy porcji soli w zalewie to ogórki będą mało-solne.

Żeby ogórki były soczyste to po podebraniu trzeba zostawić je w cieniu w zimnej wodzie na dwie godziny.

Najlepiej użyć soli kamiennej.

Do słoika można włożyć liść dębu, czarnej porzeczki lub wiśni by nadać ogórkom inny smak.
