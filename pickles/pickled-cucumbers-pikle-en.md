---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Pickled Cucumbers - Pikle}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](pickled-cucumbers-pikle.jpg){width=40%}

# Ingredients
- Cucumbers 
- Dill
- Garlic
- Horseradish
- Allspice
- Mustard seeds
- Pepper
- Onion (Optional)

# Solution
- 5 Cups of water
- 2 Cups of vinegar
- 6 Teaspoon of sugar
- 5 Tablespoon of honey
- 3 Tablespoon of salt

\End{multicols}

# Preparation

Peel the cucumbers and cut lengthwise in quarters. Deseed them and put them in a pot, sprinkle some salt on top and leave for 24 hours.

To make a solution, first melt the sugar and honey in warm water by cooking it but not boiling. Add the rest of the solution ingredients in and mix well.

Cucumbers can be cut further into bite size chunks, especially if you put them in small jars. Add dill, clove of garlic, a bit of fresh horseradish, 2 allspice seeds, a teaspoon of mustard seeds, a bit of pepper and a bit of onion cut in stripes. Fill with the solution and pasteurize for no longer than 5 minutes, in a pot filled with water, but no more than about half of jar's height.

# Notes

There are no "correct" proportions when it comes to the ingredients, add as much as you want to get the desired taste. The solution should be kept in proportions provided as much as you need for the whole batch.

To make the cucumbers more juicy, put them in cold water and leave them in shade for 2 hours right after picking.

It is best to use rock salt.

