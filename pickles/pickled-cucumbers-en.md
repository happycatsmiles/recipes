---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Pickled Cucumbers}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---
\Begin{multicols}{2}

![](pickled-cucumbers.jpg){width=40%}

# Ingredients
- Dill
- Garlic
- Horseradish
- Whole Cucumbers (small or medium sized)

# Solution
- 1L water
- 1 heapful table spoon of salt or two flat table spoons

\End{multicols}

# Preparation
Mix the solution. Put cucumbers into jars tightly packed, add a bit of dill, garlic and horseradish, then fill up with the solution. Jars should be put away into warm and dark place until the water within goes murky, then the jars should be put away into a cold place.

# Notes
To make the cucumbers more juicy, put them in cold water and leave them in shade for 2 hours right after picking.

Jars can be covered in a blanket and put in a kitchen, under the table for example. It is important to not let them stay in warmth for too long after the water goes murky as they may go bad.

There are no "correct" proportions when it comes to the ingredients, add as much as you want to get the desired taste. The solution should be kept in proportions provided as much as you need for the whole batch.

If you add half of the salt into the solution instead of whole portion, you will get low-salt cucumbers instead.

It is best to use rock salt.

You can add an oak, black currant or cherry leaf into the jars to change the flavor.
