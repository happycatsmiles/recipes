---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Ogórki w Papryce}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---
\Begin{multicols}{2}

![](pickled-cucumbers-paprika-flavor.jpg){width=40%}

\columnbreak

# Składniki
- Ogórki (Większe ogórki należy poćwiartkować)
- Ziele Angielskie
- Gorczyca

# Zalewa
- 3 szklanki wody
- 1 szklanka octu
- 1 szklanka cukru
- 2 łyżki soli
- 2 łyżeczki papryki

\End{multicols}

# Przygotowanie

Ogórki należy umieścić w słoikach, do każdego słoika należy dodać dwa ziarka ziela angielskiego i łyżeczkę gorczycy. Można do słoików dodać żywą paprykę pociętą w paski, po kilka pasków na słoik, w takim przypadku nie dodajemy już papryki do zalewy.

Zalewe należy wymieszać w garnku na ciepło by lepiej rozpuścić cukier ale nie gotować. Po wymieszaniu zalać ogórki, zamknąć i zapasteryzować gotując w sporym garnku, zalanym do połowy słoików. Ogórki powinny zostać w gotującej się wodzie przez około 10-15 minut. Po zapasteryzowaniu słoiki odstawić do ostygnięcia, a następnie przenieść w zimne miejsce. Ogórki muszą się ostać przez co najmniej 3 dni zanim będą gotowe do sporzycia.

# Notatki

Żeby ogórki były soczyste to po podebraniu trzeba zostawić je w cieniu w zimnej wodzie na dwie godziny.

Trzeba zwrócić uwagę na to czy słoiki dobrze się zamkną przy pasteryzacji, w razie potrzeby wymienić zakrątke.

Można użyć różnego rodzaju papryki, na przykład chilli.

