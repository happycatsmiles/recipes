# Oatmeal Cookies

* 1/2 cup soft butter
* 1 cup sugar (or fructose)
* 2 eggs
* 2 teaspoons vanilla extract
* 2 cups all purpose flour
* ½ teaspoon baking soda
* 2 teaspoons backing powder.
* 1 cups quick oat meal
* 2 teaspoons cinnamon
* 1/2 teaspoon nutmeg
* Optional: 1/2 cup chocolate chips

Mix all ingredients together well.

Bake at 375F for 7 to 10 minutes.
