# Corn Bread

* 1 cup corn meal
* 1 cup flour
* 1/4 cup sugar (or 2 tablespoons of fructose)
* 3 teaspoons baking powder
* 1 teaspoon salt
* 1 egg
* 1 cup milk
* 1/4 cup vegetable oil or melted butter

Combine ingredients.
Stir lightly.
Pour into greased pan.

Bake 425F for 20-25 minutes.
