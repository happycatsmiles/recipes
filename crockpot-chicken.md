# Crock Pot Chicken

* 4 to 6 chicken breasts
* 1 teaspoon chicken bouillon
* 1 teaspoon garlic salt
* 1/2 teaspoon black pepper
* 1/2 teaspoon ground cumin
* 1/2 teaspoon ground coriander
* 1 cup chopped onions
* 1/3 cup water
* 1 tablespoon olive oil

1. Chop onion.
1. Place onion on the bottom of the crock pot as a bed for the chicken.
