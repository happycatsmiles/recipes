# Basic Cooked Vegetables

Cooking tutorial for a friend that doesn't know how to cook.

## Preparation

It’s important to prepare everything beforehand.
Things cook fast and there is no time to be slicing vegetables or doing other preparation while things are cooking.

I use a food processor to slice vegetables in bulk and store them in plastic containers in the refrigerator for cooking over the next 3-4 days.
Not all vegetables can survive this,
but onions,
carrots and bell peppers (green and red) work well if they’re fresh.

If you don’t do this, you’ll need to slice the vegetables by hand.
This can be time consuming and boring.

I don’t measure things out.
I put in what looks right.
Having said that,
for a one-person meal,
you’ll need approximately

* 1 onion
* 1 carrot
* 1 bell pepper

Get out the following to be ready

* Minced garlic (about 1 medium clove or the equivalent of minced garlic)
* Lemon juice
* Hot sauce
* Vegetable oil (not olive oil)

## Cooking the Vegetables

1. Take a medium saucepan and set the stove to medium high heat.
2. Pour in just enough vegetable oil
   so you can pick up the pan and tip it around to cover the bottom of the pan with oil.
3. Put garlic in.
4. When the oil is hot enough to hear the garlic sizzle a little,
   turn the heat down to medium.
   This is very important so the vegetables don’t get overcooked.
   Higher heat doesn’t mean faster cooking.
   It just means burnt vegetables.
5. Cover the bottom of the pan with a layer of onions.
   Don’t be shy.
6. Put in about two teaspoons of lemon juice for flavor.
7. Sprinkle hot sauce on the onions.
   Don’t use too much, enough for flavor.
8. Stir everything so the hot sauce and lemon juice is evenly distributed.
9. Spread them across the bottom of the pan so they’ll cook evenly.
10. Put a layer of carrots on top.
11. Cover the pan and let the vegetables cook.
    It will take five or more minutes for the onions
    to cook completely and lose their strong raw onion flavor.
12. When the other part of the meal is ready,
    turn the heat up to high and toss in about a half of the chopped bell pepper.
    Keep what you don’t use for next time.
    Don’t let the vegetables sit or you’ll burn them.
    Stir stir stir stir!
    Let the peppers get heated up.
    This will be about **one minute** of high heat, then turn it off.

## Adding Meat

If you’re a meat person, this can be adapted.

Optionally pre-cooked meat like leftover chicken can be added.
I would wait after the onions had a few minutes to cook, during step 11.

If you want to cook raw meat,
then the procedure is different.
The meat is added in step 3 and medium high heat is used.
I add the onions when it’s about cooked all the way through,
then turn the heat down per step 4.
Follow the rest of the instructions.

## Using the Vegetables

This mixture can be used as a base for several things.

### Ramen

Plain cheap chicken ramen (with the flavor packet)
can be cooked at the same time as the vegetables.

Just cook the ramen normally how you like.
Dump in the vegetables and stir it all up.

### Burritos

I’ll cook the vegetables as above, maybe a little more hot sauce for taste.

1. Take two large tortillas,
   put them on a plate,
   spread some refried beans down the middle,
   heat in the microwave for 30 seconds if the tortillas or beans were in the refrigerator.
2. Split the vegetables between the two burritos.
   There may be more vegetables than can fit in the tortilla.
   Use these as a side dish.
3. Roll up the tortilla and cover with salsa.

I’ll usually just eat one of the burritos and share the other with my spouse.

### Spaghetti Sauce

When the vegetables are cooked, put in 3 or so cans of tomato sauce, and add spices.
I like a lot of basil and a modest amount of oregano,
then turn the heat down to low and let it simmer for the spices.
At this stage I’ll taste it to see if it needs a bit of salt.

For spaghetti sauce, I’ll usually put at least a whole bell pepper in.
I like basil and green peppers.

