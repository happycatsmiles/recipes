# Pierogi

## English

### The Dough

* Wheat flour 1kg (about 7 1/4 cups)
* One egg
* Warm water 0.5L (2 cups)
* Canola oil 2 tablespoons
* Pinch of salt

Make a mound out of the flour and create a hole in the center,
making a volcano.
Add egg, oil and salt into it.
Carefully start adding the water,
mixing it with the flour and other ingredients.
After combining, knead the dough.
Roll it out to approximately 0.5cm and cut circles out of it,
a cup can be useful for that.
Knead what will leave after cutting and
continue the process until there is no dough left to cut.

Hold your palm half folded,
creating a boat of some sort and put a roundel of dough on it.
Put the filling on top.
Fold the dough gently pushing the filling inside it.
Press the rim, closing off the pieróg.
Round its shape and correct the rim as needed, take care to not break the dough so its contents won’t spill out.
After you finish forming raw pierogi, you can advance to boiling them.

Throw into salted boiling water.
Stir and boil in deep water until pierogi start to surface, scooping them out and placing next batch.

### Filling

#### Pierogi ruskie (Russian pierogi)

* White cheese 1kg
* Potato 1kg
* One onion
* Salt, pepper

Cook the potatoes.
Run all ingredients through a meat grinder.
Spice it and roll into small balls.
Onion is for the taste and can be added in more or less quantity.

#### Pierogi szwajcarskie (Swiss pierogi)

* Cheese 1kg
* Potato or groats 1kg
* One onion
* Salt, pepper

Cook the potatoes or groats.
Run all ingredients through a meat grinder.
Spice it and roll into small balls.
Onion is for the taste and can be added in more or less quantity.

#### Pierogi z kapustą (Pierogi with sauerkraut)

* Sauerkraut 5kg
* Mushrooms (portobello mushroom is a common choice) 0.5kg
* Onion
* Marjoram, salt

Cook the sauerkraut, rinse and squeeze.
Fry mushrooms and onion.
Cool everything down and run through a meat grinder with large holes.
Spice with salt and herbs.
Scoop with a spoon onto the dough rondels.

#### Pierogi z grochem (Pierogi with beans)

* Beans such as soybeans, lentils and buckwheat 1kg
* Goulash meat or offal 20dg
* Potatoes 10dg
* Onion
* Marjoram, garlic, pepper and salt

Cook the beans, meat and potatoes individually and strain.
Fry the onion.
After cooling, run everything through meat grinder with small holes.
Mix, spice and roll into small balls.

#### Pierogi z owocami (Pierogi with fruits)

* Fruits 2kg
* Sugar 20dg

If needed, dice or cut the fruits so they fit on the dough rondel.
Before folding it in, sprinkle it with some sugar.

#### Pierogi z serem na słodko (Sweet cheese pierogi)

* White cheese 2kg
* 3 egg yolks
* Sugar 20dg

Run cheese through a meat grinder, add yolks and sugar. Mix and roll into small balls.

### Notes

"Some fillings like apple or cabbage are good when added 1dg of yeast to the dough.
Yeast should be prepared before mixing it into the volcano.
Pierogi with this type of dough can be better fried rather than boiled.

"I think that in US more popular is granulated yeast,
it is practically not used here in Poland.
We use those spongy blocks of compressed fresh yeast if that makes a difference.

[A video of a guy cooking pierogi][1] and narrating in English.
"It's pretty legit."
 https://youtu.be/GQ0GiTKzu38

[1]: https://youtu.be/GQ0GiTKzu38

"This is not hard. It just takes some time.
Over here, it's kind of a family activity to cook pierogi.
Everyone gathers, one person kneads the dough while others wrap the pierogi.
Then, when the pierogi start piling up, one person starts cooking them.

"You throw pierogi into a boiling water and when they start floating to the surface they are ready to eat.
Usually you throw a whole batch into the pot and put the cover onto it, then wait for all of them to raise to scoop them out.

"It's less work intensive and you can wrap more pierogi while the ones cook but you can overcook them.
I don't cover the pot and scoop out each one as they pop up while putting new ones in.
 So it's a constant in and out stream of pierogi.
No risk of overcooking them but you have to stand watch.

"The potato and cheese ones are great with either sour cream on top
or with diced meat and onion fried on butter.
Diced onion fried on butter is also good if you feel vegetarian.
It is important to fry it on butter and not on oil tho, since you want to pour the melted butter on top of the pierogi together with the onion.

"For fruit filled pierogi the best is either [cream (śmietana)] alone
or beat [cream with] some sugar to make a whipped cream out of.
Best is bit heavier cream so it's not too thin."

## Polish

### Ciasto

* Mąka pszenna 1kg
* Jajko 1 sztuka
* Woda ciepła 0.5L
* Olej rzepakowy 2 łyżki
* Szczypta soli

Zrobić wulkan z mąki i wbić do środka jajko i dodać olej i szczyptę soli. Ostrożnie dolewać wodę i mieszać. Zagnieść gdy się połączy. Po zagnieceniu rozwałkować na 0.5cm i wykroić krążki. Ciasto pozostałe po wykrojeniu zagnieść od nowa i wykroić kolejne krążki, powtórzyć aż użyje się całe ciasto.

Umieścić krążek na dłoni zagiętej w łudkę i do środka nałożyć farsz. Zawinąć ugniatając farsz w dołku i składając ciasto w dłoni. Zagnieść szef zamykając pieroga. W razie potrzeby poprawić kształt i szwa, pilnować by nie przebyć ciasta.

Wrzucić do gotującej się osolonej wody, zamieszać i gotować w głębokiej wodzie aż wypłynie na wieszk.

#### Notatki

Niektóre farsze takie jak jabłko czy kapusta lubią gdy doda się 1dg drożdży do ciasta. Drożdże należy rozrobić w wodzie i dodać do wulkanu przed zagnieceniem. Tego typu pierogi są mogą być lepsze smażone raczej niż gotowane.

### Farsz

#### Pierogi ruskie

* Twaróg 1kg
* Ziemniaki 1kg
* Cebula 1 sztuka
* Sól, pieprz

Ugotować ziemniaki, i wszystkie sładniki przepuścić przez maszynkę do mielenia. Dosmaczyć i poporcjować w kulki. Cebula jest do smaku i można dodać mniej lub więcej zależnie od preferencji.

#### Pierogi szwajcarskie

* Ser zółty 1kg
* Ziemniaki lub kasza 1kg
* Cebula 1 sztuka
* Sól, pieprz

Ugotować ziemniaki lub kasze, i wszystkie sładniki przepuścić przez maszynkę do mielenia. Dosmaczyć i poporcjować w kulki. Cebula jest do smaku i można dodać mniej lub więcej zależnie od preferencji.

#### Pierogi z kapustą

* Kapusta kwaszona 5kg
* Grzyby (pieczarki na przykład) 0.5kg
* Cebula
* Majeranek, sól

Ugotować kapustę, spłukać i wycisnąć. Usmażyć grzyby z cebulą. Wszystko ostudzieć i przekręcić przez maszynkę o grubych oczkach. Dosmaczyć majerankiem i solą. Nakładać łyżeczką na ciasto.

#### Pierogi z grochem

* Groch taki jak soja, soczewica lub kasza 1kg
* Mięso gulaszowe lub podroby 20dg
* Ziemniaki 10dg
* Cebula
* Majeranek, czosnek, pieprz i sól

Ugotować groch, mięso i ziemniaki (osobno) i odsedzić. Usmażyć cebulę. Po ostudzeniu przekręcić wszystko przez maszynkę z drobnymi oczkami. Wymieszać, dosmaczyć i zagnieść w kulki.

#### Pierogi z owocami

* Owoce 2kg
* Cukier 20dg

W razie potrzeby poporcjować owoce by mieściły się w cieście. Przed zawinięciem posypać cukrem.

#### Pierogi z serem na słodko

* Twaróg 2kg
* 3 żółtka z jajka
* Cukier 20dg

Twaróg skręcić przez maszynkę, dodać jajko i cukier, wymieszać i zlepić w kulki.
