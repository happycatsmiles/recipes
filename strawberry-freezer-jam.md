# Strawberry Freezer Jam

* 4 cups cleaned, cored, sliced strawberries
* 2 cups water
* 1 cup sugar
* 1 tablespoon fruit-fresh powder
* 2 tablespoon pectin powder
* 1 tablespoon citric acid powder

Prepare strawberries by washing, removing stems, and slicing them.

Put all ingredients into a sauce pan.
Cook on medium.
Stir regularly to prevent the mixture from burning.
Smash berries when they soften.
Continue cooking on low until the mixture has started to thicken.

Note: The mixture will thicken even more when cooled.
