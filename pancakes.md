# Pancakes

* 1 1/4 cup flour
* 1 tablespoon baking powder
* 1 tablespoon sugar
* 1/2 teaspoon salt

Blend In:

* 1 egg
* 1 cup milk
* 2 tablespoons vegetable oil

## Notes

* Salt can be omitted.

Source: Grandma K.
