# Macaroni and Beef

* 1/2 pound hamburger
* 1/2 small onion
* 1 small can of tomato sauce
* 1/2 tablespoon Worcestershire sauce
* 1/4 cup water
* 1/4-1/2 teaspoon chili powder
* Optional: chopped celery

Add to about a cup of cooked macaroni.

Can top with grated cheese.
