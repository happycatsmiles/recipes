---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Honey Water}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](honey-water.jpg){width=40%}

# Usage
Honey water is used to remedy stomach issues, primarily indigestion but also helps with stomach ulcers or issues related with food intolerance.

Shouldn't be used more than once a day.

# Ingredients
- Glass of water
- Teaspoon of honey

# Preparation
Add a teaspoon of honey to a glass with warm water and mix until it dissolves. Leave it for about 8 hours before drinking.

\End{multicols}

# Notes
It is best to prepare a glass of honey water in the evening, before going to bed and drink it in the morning.

When preparing, it is useful to leave the glass 1/3 empty when preparing and add warm water just before drinking to avoid drinking cold water and risking throat cold.
