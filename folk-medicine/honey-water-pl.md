---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Woda z Miodem}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](honey-water.jpg){width=40%}

# Zastosowanie
Wodę z miodem przede wszystkim stosuje się na problemy z żołądkiem takie jak niestrawności ale pomaga też przy wrzodach żołądka lub z problemami nietolerancji niektórych pokarmów.

Nie należy pić więcej niż raz dziennie.

# Składniki
- Szklanka wody
- Łyżeczka miodu

# Przygotowanie
Do szklanki ciepłej ale nie wrzącej wody wrzucić łyżeczkę miodu i wymieszać do rozpuszczenia. Zostawić na około 8 godzin.

\End{multicols}

# Notatki
Najlepiej jest przygotować wodę wieczorem przed spaniem i wypić rano na czczo.

Można zostawić około 1/3 szklanki pustej przy przygotowaniu żeby można było potem dolać trochę ciepłej wody przed wypiciem by uniknąć przeziębienia gardła.
