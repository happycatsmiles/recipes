---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Nuggetsy z kurczaka}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](chicken-nuggets.jpg){width=40%}

# Składniki
- 0.5kg Piersi z kurczaka 
- 1 Jajko
- Panierka
- Dowolne Przyprawy


# Przygotowanie
Rozbij jajko i wymieszaj je z przyprawami w misce. Piersi umyj i pokrój na niewielkie kawałki, wrzuć do miski z jajkiem i dobrze wymieszaj. Zwróć uwagę by odkraiwać wszelkie chrząstki, kostki i inne nieczystości jakie mogłyby się znaleźć na mięsie. Pozostaw na około godzinę by kurczak przeszedł przyprawami. 

Przygotuj talerz na którym będziesz obtaczać mięso w panierce. Do sporej patelni nalej oleju tak by jego ilość topiła kawałki mięsa przynajmniej w połowie ale nie więcej. Rozgrzej olej na średnio wysokim ogniu i ciasno rozłóż mięso tak by zmieścić jak najwięcej pozostawiając wystarczająco miejsca by można było je obracać. Mięso obracaj regularnie, co około minute lub dwie tak by się nie przepaliło. Usmażone kawałki odkładaj na osobny talerz lub do garnuszka.

\End{multicols}

# Notatki
Piersi można kroić na różne wielkości i kształty dowolnie ale kawałki powinny być płaskie gdyż inaczej trudno je dosmażyć i środki mogą być surowe.

Można przygotować większą ilość niż jest podane jeśli zachowa się proporcje.

Trzeba zwrócić uwagę że różnej wielkości kawałki będą się smażyć w innym tempie i należy je zdejmować z patelni i zastępować nowymi gdy te już dojdą.

Żeby sprawdzić czy olej jest już wystarczająco gorący, można wrzucić na niego troszkę panierki i jeśli będzie syczeć i się pienić to jest już gotowy.

Trzeba zwrócić uwagę na to by kurczak się usmażył właściwie. Dosyć łatwo jest go nie dosmażyć jak i przepalić. Umiejętność ta przyjdzie z doświadczeniem. Można sobie pomóc kłując kurczaki widelcem, jeśli widelec łatwo wchodzi w kurczaka to jest już gotowy. Zamiast widelca można użyć pałeczki jeśli nie chce się zadrapać patelni.

Ważne jest żeby pilnować oleju, gdyż ten będzie wyparowywał i jeśli będzie go za mało, to piersi będą się łatwo przepalać zanim się usmażą.

Jeśli wymiesza się panierkę z mąką ziemniaczaną to utworzy ona bardziej chrupką skorupkę wokół nuggetów.
