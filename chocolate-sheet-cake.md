# Texas Cake

Oven 350F (175C)

* 2 cups flour
* 2 cups sugar
* 1 cup melted butter
* 1/2 cup milk
* 1 teaspoon vanilla
* 1/4 cup cocoa
* 2 teaspoons baking soda
* 2 eggs
* 1 cup water
  (Add this last,
  a little at a time,
  to get good cake consistency,
  though often I use milk).

Mix together for smooth cake batter.

## Round Cake Version

Pour into greased and floured cake pans.

Bake for 30-35 minutes.

## Sheet Cake Version

Cover sheet pan
(cookie sheet with raised sides)
with butter and light flour.

Bake for 20 minutes.

## Optional Frosting

I don’t have good measurements for this. I tend to make frosting by feel.

* 2-4 tablespoon of butter
* 2 tablespoons of cocoa

Melt together

* Add 1 teaspoon vanilla.

Keep adding powdered sugar and small amounts of milk to make the amount needed.
I think I start with 2 cups of powdered sugar.

Source: Grandma K.

