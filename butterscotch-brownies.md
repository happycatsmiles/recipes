# Butterscotch Brownies

* 1/4 cup butter
* 1 cup brown sugar
* 1 egg
* 3/4 cup flour
* 1 teaspoon baking powder
* 1 teaspoon vanilla
* Optional: chopped nuts, chocolate chips, or coconut

Pour into a greased pan.

Bake 350F for 25 minutes.
