---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Panierka}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](breading.jpg){width=40%}

# Składniki
- Bułka, dowolna


# Przygotowanie
Bułkę należy ułożyć w pudełku lub na tacce i ustawić na włączonym piekarniku i pozostawić aż bułka się zeschnie do chrupkiej twardości.

Zeschniętą bułkę należy przetrzeć na tartce lub przekręcić przez maszynkę. Druga opcja daje lepszy rezultat. Po utarciu panierkę należy zużyć lub zamknąć w szczelnym pojemniku.

\End{multicols}

# Notatki
Przed suszeniem bułki najlepiej jest ją pokroić na plastry lub mniejsze części, gdyż zdrobnienie jej po wysuszeniu może być trudniejsze.

Najlepiej jest użyć zwykłej bułki, inne pieczywa zwykle nadają niepożądane smaki.

Do panierki można dodać różnych przypraw przed użyciem ale mięso obtoczone taką panierką nie nabierze smaku z panierki. Można to wykorzystać jeśli chce się nadać różne smaki mięsu i panierce.

Suszyć bułkę najlepiej przy okazji gdy się robi różne wypieki.

W sezonach gdy pali się w piecu, można suszyć bułkę na piecu.

Zamiast bułki można zetrzeć chrupki kukurydziane ale te są bardzo łatwe do spalenia.

Do panierki można dodać sezam, lub bezpośrednio panierować w sezamie. Sezam jest najlepszy jeżeli się go trochę podpali przy smażeniu.

Pozostała panierka która miała kontakt z mięsem powinna zostać wyrzucona.
