# Tuna Turnovers

## Ingredients

### Pastry

* 1 cup flour
* 1/2 teaspoon salt
* 1/3 cup shortening

### Filling

* 1 can tuna
* 1/4 cup mayonnaise
* some finely-chopped onion
* 1/2 teaspoon worcestershire sauce.

## Instructions

Cut shortening into flour.

Add a spoonful of water at a time,
stirring with a fork
until the dough is moist but not sticky
(about three tablespoons).

Roll or pat into a 12" square.
Cut the dough into four squares.

Spread the meat mixture on each square
and fold diagonally, into a triangle.
Seal the edges with the tines of a fork.

Bake at 400F for 20 minutes.

Serve with a warm soup mixture.
Commonly we use cream of mushroom soup.

Source: Grandma K.
