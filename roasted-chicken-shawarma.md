# Roasted Chicken Shawarma

* 2 pounds skinless chicken thighs (cut into 12 pieces)
* 1 onion (large, peeled and quartered)
* 2 tablespoons olive oil
* 6 garlic cloves (minced)
* 2 lemons
* 2 teaspoons ground cumin
* 2 teaspoons paprika
* 1 teaspoon tumeric
* 1 teaspoon salt
* 2 teaspoons pepper
* 1 pinch cinnamon
* 2 tablespoons water

1. In a large bowl, combine lemon juice, olive oil, water, garlic, cumin, paprika, tumeric, salt, pepper and cinnamon. Mix well, then add chicken, and toss well to coat all pieces evenly. Cover and refrigerate overnight or for a minimum of 2 hours.
2. Preheat oven to 425, and mist a 11” x 13” baking dish with nonfat cooking spray.
3. Mix onions into the chicken and marinade, then spread all pieces evenly into the baking dish.
4. Place in oven and roast, uncovered, for about 30-40 minutes, or until chicken cooked through and is browned and crisped on the edges.
5. Remove from oven and let cool for 5 minutes. Slice into strips and serve inside pita bread or along a side of steamed rice.

<img src="roasted-chicken-shawarma.jpg" width="50%">
