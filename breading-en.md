---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Breading}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](breading.jpg){width=40%}

# Ingredients
- Any bread roll


# Preparation
Put the bread rolls in a cardboard box or on a tray and set on top of an oven. Leave it there until it dries out.

Dry bread rolls should be grinded into dust either using a grater or a mechanical grinder. The latter gives finer grain which is easier to use. After making the breading, either use it or put into a sealed container.

\End{multicols}

# Notes
Before you dry the bread roll, cut it into slices or smaller chunks as it is easier to do so while it is soft.

Using regular wheat flour bread rolls is preferable, as any other type of bread tends to give the breading undesirable taste. Definitely don't use anything sweetened or with filling.

You can add additional spices to the breading before use, however, meat coated in the breading won't absorb the taste. This can be used if you want to give your meat and breading varying tastes.

Usually it's the best to dry bread rolls while using the oven for baking other things.

During cold seasons, it is useful to use your furnace to dry the bread rolls, setting a cardboard box filled with them on top of your furnace.

You can use corn based breading, however, keep in mind that it will burn much easier than bread roll based one.

You can add sesame to the breading, or use it on its own. Sesame tends to work best if you slightly burn it.

Do not reuse breading that already had contact with meat.
